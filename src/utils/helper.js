
let sorting = (array) => {
    array.sort()
    return array;
}

let compare = (a, b) => {
    return a['PM2.5'] - b['PM2.5'];
}

let average = (nums) => {
    let sum = 0;
    for (let i = 0; i < nums.length; i++){
        sum += nums[i];
    }
    let num = (sum / nums.length) || 0;
    num = Math.round((num + Number.EPSILON) * 100) / 100;
    return num;
}


module.exports = {
    sorting,
    compare,
    average
}
