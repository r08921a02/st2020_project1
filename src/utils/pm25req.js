const axios = require('axios');

const sensor_url = 'https://opendata.epa.gov.tw/api/v1/REWIQA/?$orderby=SiteName&$skip=0&$top=1000&format=json'; 


var getPM25 = (place) => {

    return new Promise((resolve, reject) => {
        axios.get(sensor_url)
            .then(function (response) {
			    // console.log(response.data);
			    response = response.data;
			    let data = [];
			    for(let i = 0; i < response.length; i++) {
			    	if(-1 !== place.findIndex((e) => e === response[i].County)) data.push(response[i]);
			    }
			    if(data) {
			        resolve(data);
			    } else {
			        reject('no sensor');
			    }
		    })
            .catch(function (error) {
			    // handle error
			    reject(error);
		});
    })

}


getPM25(['臺北市', '新北市', '臺中市']).then((data) => {
	console.log(data)
});


module.exports = {
    getPM25
}
